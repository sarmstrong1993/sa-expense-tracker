import Expenses from './components/Expenses/Expenses';
import NewExpense from './components/NewExpense/NewExpense';
import { useState } from 'react';

const DUMMY_EXPENSES = [
  {
    id: 'e1',
    title: 'Car Insurance',
    amount: 294.67,
    date: new Date(2021, 2, 28),
  },
  {
    id: 'e2',
    title: 'Toilet Paper',
    amount: 5.99,
    date: new Date(2021, 5, 12),
  },
  {
    id: 'e3',
    title: 'Computer',
    amount: 1200.0,
    date: new Date(2020, 7, 15),
  },
  {
    id: 'e4',
    title: 'Car Insurance',
    amount: 312.84,
    date: new Date(2020, 2, 28),
  },
];

const App = () => {
  const [expenses, setExpenses] = useState(DUMMY_EXPENSES);

  const addExpenseHandler = (expense) => {
    setExpenses((prevExpenses) => {
      return [expense, ...expenses];
    });
  };

  return (
    <div>
      <NewExpense onAddExpense={addExpenseHandler} />
      <Expenses expenses={expenses} />
    </div>
  );
};

export default App;
